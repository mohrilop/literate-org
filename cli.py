import click

@click.command()
@click.option('-s', '--src', default="src")
@click.option('-b', '--build', default="code")
def lit(src, build):
    click.echo("welcome to the literate tool")
    click.echo(f"src: {src} | build: {build}")

    
if __name__ == "__main__":
    lit()
